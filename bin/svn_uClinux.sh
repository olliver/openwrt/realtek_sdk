#!/bin/sh
#
#
#

ROOT_PWD=`pwd`
date=`date +%Y%m%d`
#DIR_PREFIX=intelligent_
DIR_PREFIX=

SVN_SERVER=http://cadinfo.realtek.com.tw/svn/CN/Switch/trunk/intelligent
OS_TYPE=uClinux
LOADER_TYPE=u-boot
BIN_DIR=bin
KERNEL_DIR=kernel
IMAGE_DIR=image
SDK_DIR=sdk
TURNKEY_DIR=turnkey
LOADER_DIR=${LOADER_TYPE}
OS_DIR=${OS_TYPE}
REALTEK_LIB_DIR=realtek
SVN_OS=${SVN_SERVER}/${KERNEL_DIR}/${OS_TYPE}
SVN_LOADER=${SVN_SERVER}/${LOADER_TYPE}
SVN_SDK=${SVN_SERVER}/${SDK_DIR}
SVN_TURNKEY=${SVN_SERVER}/${TURNKEY_DIR}
SVN_BIN=${SVN_SERVER}/${BIN_DIR}
SVN_REALTEK=${SVN_SERVER}/${REALTEK_LIB_DIR}

FINAL_DIR=${DIR_PREFIX}${OS_TYPE}_${date}

case "$1" in
  -h)   
	echo "usage: $0 [-h] [-r RUNTIME_REV] [-l LOADER_REV]"; echo ""
	echo "  If no REV is specified, the latest revision of source code will be checked out."; echo ""
	echo "option:" 
	echo "  -h : help on the usage of this script."
	echo "  -r : if RUNTIME_REV is specified, it determines in which revision the runtime source is checked out."
	echo "  -l : if LOADER_REV is specified, it determines in which revision the loader source is checked out."
	echo ""
	exit 1
esac

# if the FINAL_DIR is existed
if [ -e ${FINAL_DIR} ]; then
	if [ -d ${FINAL_DIR} ]; then
		cd ${FINAL_DIR}
	else
		mkdir -p ${FINAL_DIR}
	fi
else
	mkdir -p ${FINAL_DIR}
	cd ${FINAL_DIR}
fi

# if FINAL_DIR is builded
CUR_PWD=`pwd`
if [ "${CUR_PWD}" == "${ROOT_PWD}" ]; then
	echo "Build folder fail: ${FINAL_DIR}"
else
	if [ -e Makefile ]; then
		cd .
	else
		svn export ${SVN_BIN}/Makefile.root.${OS_TYPE}
		mv Makefile.root.${OS_TYPE} Makefile
	fi

	if [ -e ${IMAGE_DIR} ] && [ -d ${IMAGE_DIR} ]; then
		cd .
	else
		mkdir -p ${IMAGE_DIR}
	fi

	if [ -e ${BIN_DIR} ] && [ -d ${BIN_DIR} ]; then
		cd .
	else
		mkdir -p ${BIN_DIR}
		svn export ${SVN_BIN}/sdk_release.sh ./${BIN_DIR}/sdk_release.sh
	fi	

	mkdir -p ${KERNEL_DIR}

case "$1" in
  -r)
	svn co -r $2 ${SVN_SDK}
	svn co -r $2 ${SVN_TURNKEY}
	cd ${KERNEL_DIR} && svn co -r $2 ${SVN_OS} && cd ..
	;;
  -l)
	svn co -r $2 ${SVN_LOADER}
	;;
esac

case "$3" in
  -l)
	svn co -r $4 ${SVN_LOADER}
	;;
  -r)
	svn co -r $4 ${SVN_SDK}
	svn co -r $4 ${SVN_TURNKEY}
	cd ${KERNEL_DIR} && svn co -r $4 ${SVN_OS} && cd ..
	;;
  *)
	if [ ! -d ${SDK_DIR} ]; then
		svn co ${SVN_SDK}
	fi
	
	if [ ! -d ${TURNKEY_DIR} ]; then
		svn co ${SVN_TURNKEY}
	fi

	if [ ! -d ${LOADER_DIR} ]; then
		svn co ${SVN_LOADER}
	fi

	cd ${KERNEL_DIR}
	if [ ! -d ${OS_DIR} ]; then
		svn co ${SVN_OS} && cd ..
	fi
	
	if [ ! -d ${REALTEK_LIB_DIR} ]; then
		svn co ${SVN_REALTEK}
	fi
	;;
esac
   
fi

