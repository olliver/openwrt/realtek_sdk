#!/bin/sh

ROOT_DIR=`pwd`
LOADER_DIR=${ROOT_DIR}/u-boot
UBOOT_2011_DIR=${ROOT_DIR}/u-boot-2011.12
KERNEL_DIR=${ROOT_DIR}/kernel
SDK_DIR=${ROOT_DIR}/sdk

KERNEL_LINUX_DIR=${KERNEL_DIR}/linux
KERNEL_UCLINUX_DIR=${KERNEL_DIR}/uClinux

KERNEL_LINUX_VER_DIR=${KERNEL_LINUX_DIR}/linux-2.6.x
KERNEL_LINUX_UCLIBC_DIR=${KERNEL_LINUX_DIR}/uClibc-0.9.27
KERNEL_LINUX_USER_SWITCH_DIR=${KERNEL_LINUX_DIR}/user/switch
KERNEL_LINUX_VENDOR_DIR=${KERNEL_LINUX_DIR}/vendors/Realtek


# The unused files of projects
RMF_LINUX_COMMON="
	${KERNEL_LINUX_VER_DIR}/arch/alpha
	${KERNEL_LINUX_VER_DIR}/arch/arm
	${KERNEL_LINUX_VER_DIR}/arch/arm26
	${KERNEL_LINUX_VER_DIR}/arch/avr32
	${KERNEL_LINUX_VER_DIR}/arch/cris
	${KERNEL_LINUX_VER_DIR}/arch/frv
	${KERNEL_LINUX_VER_DIR}/arch/h8300
	${KERNEL_LINUX_VER_DIR}/arch/i386
	${KERNEL_LINUX_VER_DIR}/arch/ia64
	${KERNEL_LINUX_VER_DIR}/arch/m32r
	${KERNEL_LINUX_VER_DIR}/arch/m68k
	${KERNEL_LINUX_VER_DIR}/arch/m68knommu
	${KERNEL_LINUX_VER_DIR}/arch/parisc
	${KERNEL_LINUX_VER_DIR}/arch/powerpc
	${KERNEL_LINUX_VER_DIR}/arch/ppc
	${KERNEL_LINUX_VER_DIR}/arch/s390
	${KERNEL_LINUX_VER_DIR}/arch/sh
	${KERNEL_LINUX_VER_DIR}/arch/sh64
	${KERNEL_LINUX_VER_DIR}/arch/sparc
	${KERNEL_LINUX_VER_DIR}/arch/sparc64
	${KERNEL_LINUX_VER_DIR}/arch/um
	${KERNEL_LINUX_VER_DIR}/arch/v850
	${KERNEL_LINUX_VER_DIR}/arch/x86_64
	${KERNEL_LINUX_VER_DIR}/arch/xtensa
	${KERNEL_LINUX_VER_DIR}/drivers/ata/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/atm/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/bluetooth/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/connector/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/cpufreq/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/crypto/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/dio/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/dma/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/edac/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/fc4/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/hwmon/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/i2c/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/ide/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/ieee1394/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/infiniband/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/isdn/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/leds/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/macintosh/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/mca/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/md/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/message/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/mmc/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/nubus/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/parport/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/pcmcia/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/rtc/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/sbus/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/scsi/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/sh/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/sn/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/tc/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/telephony/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/usb/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/w1/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/zorro/*.c
	${KERNEL_LINUX_VER_DIR}/drivers/ata/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/atm/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/bluetooth/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/connector/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/cpufreq/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/crypto/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/dio/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/dma/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/edac/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/fc4/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/hwmon/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/i2c/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/ide/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/ieee1394/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/infiniband/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/isdn/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/leds/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/macintosh/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/mca/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/md/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/message/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/mmc/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/nubus/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/parport/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/pcmcia/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/rtc/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/sbus/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/scsi/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/sh/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/sn/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/tc/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/telephony/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/usb/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/w1/*.h
	${KERNEL_LINUX_VER_DIR}/drivers/zorro/*.h
	${KERNEL_LINUX_VER_DIR}/include/asm-alpha
	${KERNEL_LINUX_VER_DIR}/include/asm-arm
	${KERNEL_LINUX_VER_DIR}/include/asm-arm26
	${KERNEL_LINUX_VER_DIR}/include/asm-avr32
	${KERNEL_LINUX_VER_DIR}/include/asm-cris
	${KERNEL_LINUX_VER_DIR}/include/asm-frv
	${KERNEL_LINUX_VER_DIR}/include/asm-h8300
	${KERNEL_LINUX_VER_DIR}/include/asm-i386
	${KERNEL_LINUX_VER_DIR}/include/asm-ia64
	${KERNEL_LINUX_VER_DIR}/include/asm-m32r
	${KERNEL_LINUX_VER_DIR}/include/asm-m68k
	${KERNEL_LINUX_VER_DIR}/include/asm-m68knommu
	${KERNEL_LINUX_VER_DIR}/include/asm-parisc
	${KERNEL_LINUX_VER_DIR}/include/asm-powerpc
	${KERNEL_LINUX_VER_DIR}/include/asm-ppc
	${KERNEL_LINUX_VER_DIR}/include/asm-s390
	${KERNEL_LINUX_VER_DIR}/include/asm-sh
	${KERNEL_LINUX_VER_DIR}/include/asm-sh64
	${KERNEL_LINUX_VER_DIR}/include/asm-sparc
	${KERNEL_LINUX_VER_DIR}/include/asm-sparc64
	${KERNEL_LINUX_VER_DIR}/include/asm-um
	${KERNEL_LINUX_VER_DIR}/include/asm-v850
	${KERNEL_LINUX_VER_DIR}/include/asm-x86_64
	${KERNEL_LINUX_VER_DIR}/include/asm-xtensa
	${KERNEL_LINUX_UCLIBC_DIR}
"

# The unused files of RTL8316S
RMF_LINUX_RTL8316S="
	${KERNEL_LINUX_VER_DIR}/drivers/net/switch/rtl8316s	
	${KERNEL_LINUX_VER_DIR}/net/switch/rtl8316s
	${KERNEL_LINUX_USER_SWITCH_DIR}/rtl8316s
	${KERNEL_LINUX_VENDOR_DIR}/RTL8316S
"

# The unused files of RTL8389
RMF_LINUX_RTL8389="
	${KERNEL_LINUX_VER_DIR}/drivers/net/switch/rtl8389	
	${KERNEL_LINUX_VER_DIR}/net/switch/rtl8389
	${KERNEL_LINUX_USER_SWITCH_DIR}/rtl8389
	${KERNEL_LINUX_VENDOR_DIR}/RTL8389
"

# The unused files of GS1528PWR
RMF_LINUX_GS1528PWR="
	${KERNEL_LINUX_VER_DIR}/drivers/net/switch/GS1528PWR	
	${KERNEL_LINUX_VER_DIR}/net/switch/GS1528PWR
	${KERNEL_LINUX_USER_SWITCH_DIR}/GS1528PWR
	${KERNEL_LINUX_VENDOR_DIR}/GS1528PWR
"

# The unused files of ES4326R
RMF_LINUX_ES4326R="
	${KERNEL_LINUX_VER_DIR}/drivers/net/switch/ES4326R
	${KERNEL_LINUX_VER_DIR}/net/switch/ES4326R
	${KERNEL_LINUX_USER_SWITCH_DIR}/ES4326R
	${KERNEL_LINUX_VENDOR_DIR}/ES4326R
"

# The unused files of PC1710
RMF_LINUX_PC1710="
	${KERNEL_LINUX_VENDOR_DIR}/PC1710
"

# The unused files of Loader
RMF_LOADER_COMMON="
	${LOADER_DIR}/include/configs/rtl8316s.h
	${LOADER_DIR}/board/REALTEK/rtl8316s
	${LOADER_DIR}/board/REALTEK/rtl8389/flash.c
"

# The unused files of SDK
RMF_SDK_COMMON="
	${SDK_DIR}/system/osal/x86
	${SDK_DIR}/system/ioal/model
	${SDK_DIR}/unittest
	${SDK_DIR}/model
"

# The unused files of SDK with Linux
RMF_SDK_LINUX="
	$RMF_LINUX_COMMON""$RMF_LINUX_RTL8389""$RMF_LINUX_RTL8316S""$RMF_LINUX_GS1528PWR""$RMF_LINUX_ES4326R""$RMF_LINUX_PC1710""
	${KERNEL_LINUX_VER_DIR}/include/asm-mips/mach-realtek/*.h
	${KERNEL_LINUX_VER_DIR}/include/asm-mips/mach-realtek/rtl8316s
	${KERNEL_LINUX_VER_DIR}/arch/mips/realtek/rtl8316s
	${KERNEL_LINUX_VER_DIR}/arch/mips/realtek/*.c
	${SDK_DIR}/src/app/lib/readline
"

# The unused files of SDK with uClinux
RMF_SDK_UCLINUX="
	$(KERNEL_UCLINUX_DIR)/vendors/Realtek/8390_SDK_DEMO
	$(KERNEL_UCLINUX_DIR)/vendors/Realtek/8390_SDK_FPGA
	${UBOOT_2011_DIR}/ddr_cli_tools/src
"

# Main
make distclean

case "$1" in
  linux)	
	for FILE in ${RMF_SDK_LINUX}
	do
		rm -fr ${FILE} >& /dev/null &
	done	
	;;
  uClinux)	
	for FILE in ${RMF_SDK_UCLINUX}
	do
		rm -fr ${FILE} >& /dev/null &
	done	
	;;
  *)
	echo ""; echo "## [**Error] Usage: $0 {linux|uClinux} ##"; echo ""
	exit 1  
esac

for FILE in ${RMF_SDK_COMMON}
do
	rm -fr ${FILE} >& /dev/null &
done	

exit 0

