# Realtek/ZyXEL source drop
This repository contains the source drop from the Realtek SDK from Zyxel for
the various XGS1* series. Each branch has the specific dump for that particular
unit.

## Back up your flash!
**Before attempting anything with your unit,**
**ensure you backup all flash partitions!!**

Backing up flash partitions can probably be best done from a net-booted
initramfs
```
setenv bootfile='openwrt-rtl930x-<target>-initramfs-kernel.bin'; rtk network on; tftpboot'
```

The initramfs image should contain network and `scp` support, so using `dd` to
download the various images. Since we cannot download the original partitions
in their original forms, `dd` all mtd partitions, and then concatenate them
on the host (`cat mtd*.bin > mtd.bin`), splitting them up again can be then
done with some `dd` magic. The bytes/offsets can be gotten from the original
U-Boot shell (`flshow` yields all addresses/sizes, with a `0xb4` offset for
example, that should be removed of course). E.g. on the 9302, the firmware is
stored at `0xb4300000` thus, the flash offset for `dd` would be `0x00300000`.

On the XGS1010-12, only the `mtd0` and `mtd5` contain actual data, all other
partitions are empty, as the XGS1010-12 is an unmanaged switch. Other switches
may contain 'calibration' data (the MAC addresses for example) which live in
`mtd1`. This partition is probably the most import for managed switches
because of this. Back up this partition! (and store the output of `printenv`
somewhere!).


## Factory images/returning to factory
The [vendor](vendor) directory contains the pre-compiled (factory) images from
the target. To replace (or restore) the factory software, first place
`u-boot.bin` on a tftp server, and on the target, run `upgrade loader`.
If this was successful, `reset` or power-cycle the unit to reboot into the
factory bootloader. Next, the environment needs to be restored. Either
by erasing the entire `BDINFO` partition using `flerase` and adding the needed
`setenv` + `saveenv` variables, or by netbooting into openwrt, and restoring
the `mtd1` partition. Using `fw_setenv` from OpenWRT is also possible.

In the factory U-Boot shell, `upgrade runtime` will download `vmlinux.bix` via
tftp and restore the factory software.


## Building the SDK
To build the SDK, the use of `Docker` (or an alternative) is recommended.
Not using `docker` is not supported.

To build the SDK build environment container image:
```
docker image build --file Containerfile --rm --tag "$(basename "$(pwd)"):latest" .
```

> __NOTE:__ The container is based on the i386 architecture, due to the
> pre-packaged toolchain being i386. There may be alternative solutions
> in the future using a 64bit container. This may require `qemu-user-static`
> and `binfmt` support. For example using the `multiarch` container.
> `docker run --rm --privileged multiarch/qemu-user-static --reset -p yes`

This container will be tagged with the name of the directory and the `latest`
postfix. E.g. in the directory named `xgs1010`, the container will be called
`xgs1010:latest`. This is probably wise when using multiple `git worktree`'s
for each of the branches. Different tag names or postfixes can be used, to
for example store a build container for later use. The name is not important
other then referring to it from the run step bellow.

To enter compile the SDK using 8 cores/jobs:
```
docker container run --rm -it -v "$(pwd):/workdir" -w '/workdir' "$(basename "$(pwd)"):latest" make --jobs 8 9300 loader
```

Important to know, is that `make 9300` is _always_ needed to be run at least
before once before any other command, because this will setup the SDK
(creating symlinks etc) as part of its run. Successive builds of loader
that does not touch `.config` can omit `9300`.

In certain cases, it may be more convenient to enter the container, which can
be easily achieved by removing `make` and its arguments.

> __Warning:__ The container currently compiles everything as root. This will
> change some permissions on the host files. This can be problematic when using
> `git clean -d --force` for example. This can be resolved either by fixing
> permissions from within the container via `chroot 1000:1000 -R .` or using
> `sudo git clean -d --force` instead.


> __NOTE:__ Only the loader `u-boot.bin` has been getting some love and
> attention. The `vmlinux.bix` has been netbooted, but was not used. Network
> functionality and diag support needs some extra 'post-setup' steps as per
> manual.

> __TODO:__ The path should be set via some other mechanism without having
> `docker run override it`


## Disclaimer
Any damage to any person, any device are all your own responsibility. No
warranty or guarantee's can be had.
