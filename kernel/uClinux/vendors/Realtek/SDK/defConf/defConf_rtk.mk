
DEFAULT_CONFIG_DIR := kernel/uClinux/vendors/Realtek/SDK/defConf

#
# For RTK internal usage, to build for each chip in its default config
#
.PHONY : $(EXTRA_HELP)

INTERNAL_PROJECTS := stack 9300fpga 9310fpga 9310.model 9300.linux 9300new newu 9300nl 83xxma 93xxs
EXTRA_HELP := internal_projects_help

$(EXTRA_HELP):
	@printf "\nExtra commands (internal projects):\n"
	@printf "   make stack              (for staking simulation)\n"
	@printf "   make 9300fpga           (9300 FPGA, RTK internal)\n"
	@printf "   make 9310fpga           (9310 FPGA, RTK internal)\n"
	@printf "   make 9310.model         (9310 model code, RTK internal)\n"
	@printf "   make 9300.linux         (9300 diag shell, RTK internal)\n"
	@printf "   make 9300new            (9300 new makefile system)\n"
	@printf "   make newu               (new makefile system user mode)\n"
	@printf "   make 9300nl             (9300 non-linux)\n"
	@printf "   make 83xxma             (83xx kernel mode with MA3421E USB chip)\n"
	@printf "   make 93xxs              (93xx stacking)\n"
	@printf "   make 8380tk             (8380 turnkey config)\n"
	@printf "   make 8390tk             (8390 turnkey config)\n"
	@printf "   make 9300tk             (9300 turnkey config)\n"

