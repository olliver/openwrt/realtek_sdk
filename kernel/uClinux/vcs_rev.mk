#
# Copyright (C) 2015 Realtek Semiconductor Corp.
#
# Makefile for SDK Diag Shell Source
#


SDK_VCS_HDR := $(SDKDIR)/system/include/common/vcs_rev.h
VCS_INFO_DIR := $(TOPDIR)

ifeq ($(VCS_REVISION),)
    SVN_INFO := $(shell svn info $(VCS_INFO_DIR) | grep "Last Changed Rev")

    #
    # Revision number from Version Control System
    #
    ifeq "$(SVN_INFO)" ""
        VCS_REVISION := "N/A"
    else
        VCS_REVISION := "$(shell svn info $(VCS_INFO_DIR) | grep 'Last Changed Rev' | sed 's/Last Changed Rev: \+//')"
        VCS_WC_REVISION := "$(shell svnversion -c $(VCS_INFO_DIR) | sed 's/.*://')"

        ifneq "$(VCS_REVISION)" "$(VCS_WC_REVISION)"
            VCS_REVISION := $(VCS_REVISION)":"$(VCS_WC_REVISION)
        endif
    endif
endif
export VCS_REVISION

#
# Revision number from header file
#
ifeq ($(SDK_VCS_HDR), $(wildcard $(SDK_VCS_HDR)))
    H_VCS_REVISION := "$(shell grep 'RT_VCS_REVISION' $(SDK_VCS_HDR) | sed 's/\#define RT_VCS_REVISION \+//')"
else
    H_VCS_REVISION := ""
endif


.PHONY: vcs_rev
vcs_rev:
	@if [ ! "$(VCS_REVISION)" = "$(H_VCS_REVISION)" ]; then \
	    echo "Update $(SDK_VCS_HDR)"; \
	    echo "/* This file is automatic generated at build time. Do NOT modify it! */"  > $(SDK_VCS_HDR); \
	    echo ""                                                                         >> $(SDK_VCS_HDR); \
	    echo "#ifndef __RT_VCS_REV_H__"                                                 >> $(SDK_VCS_HDR); \
	    echo "#define __RT_VCS_REV_H__"                                                 >> $(SDK_VCS_HDR); \
	    echo ""                                                                         >> $(SDK_VCS_HDR); \
	    echo "#define RT_VCS_REVISION              \"$(VCS_REVISION)\""                 >> $(SDK_VCS_HDR); \
	    echo ""                                                                         >> $(SDK_VCS_HDR); \
	    echo "#endif/* __RT_VCS_REV_H__ */"                                             >> $(SDK_VCS_HDR); \
	    echo ""                                                                         >> $(SDK_VCS_HDR); \
	fi


.PHONY: vcs_rev-distclean
vcs_rev-distclean:
	rm -f $(SDK_VCS_HDR)

