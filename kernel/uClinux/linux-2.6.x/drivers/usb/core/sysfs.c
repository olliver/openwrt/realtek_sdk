/*
 * drivers/usb/core/sysfs.c
 *
 * (C) Copyright 2002 David Brownell
 * (C) Copyright 2002,2004 Greg Kroah-Hartman
 * (C) Copyright 2002,2004 IBM Corp.
 *
 * All of the sysfs file attributes for usb devices and interfaces.
 *
 */


#include <linux/kernel.h>
#include <linux/usb.h>
#include "usb.h"

#ifdef CONFIG_USB_HCD_TEST_MODE
#define REG32(reg)      (*(volatile unsigned int *)((unsigned int)reg)) /*Add by Switch*/
#define pr_debug        printk
#endif /* CONFIG_USB_HCD_TEST_MODE */

/* Active configuration fields */
#define usb_actconfig_show(field, multiplier, format_string)		\
static ssize_t  show_##field (struct device *dev,			\
		struct device_attribute *attr, char *buf)		\
{									\
	struct usb_device *udev;					\
	struct usb_host_config *actconfig;				\
									\
	udev = to_usb_device (dev);					\
	actconfig = udev->actconfig;					\
	if (actconfig)							\
		return sprintf (buf, format_string,			\
				actconfig->desc.field * multiplier);	\
	else								\
		return 0;						\
}									\

#define usb_actconfig_attr(field, multiplier, format_string)		\
usb_actconfig_show(field, multiplier, format_string)			\
static DEVICE_ATTR(field, S_IRUGO, show_##field, NULL);

usb_actconfig_attr (bNumInterfaces, 1, "%2d\n")
usb_actconfig_attr (bmAttributes, 1, "%2x\n")
usb_actconfig_attr (bMaxPower, 2, "%3dmA\n")

static ssize_t show_configuration_string(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct usb_device *udev;
	struct usb_host_config *actconfig;

	udev = to_usb_device (dev);
	actconfig = udev->actconfig;
	if ((!actconfig) || (!actconfig->string))
		return 0;
	return sprintf(buf, "%s\n", actconfig->string);
}
static DEVICE_ATTR(configuration, S_IRUGO, show_configuration_string, NULL);

/* configuration value is always present, and r/w */
usb_actconfig_show(bConfigurationValue, 1, "%u\n");

static ssize_t
set_bConfigurationValue (struct device *dev, struct device_attribute *attr,
		const char *buf, size_t count)
{
	struct usb_device	*udev = to_usb_device (dev);
	int			config, value;

	if (sscanf (buf, "%u", &config) != 1 || config > 255)
		return -EINVAL;
	usb_lock_device(udev);
	value = usb_set_configuration (udev, config);
	usb_unlock_device(udev);
	return (value < 0) ? value : count;
}

static DEVICE_ATTR(bConfigurationValue, S_IRUGO | S_IWUSR,
		show_bConfigurationValue, set_bConfigurationValue);

/* String fields */
#define usb_string_attr(name)						\
static ssize_t  show_##name(struct device *dev,				\
		struct device_attribute *attr, char *buf)		\
{									\
	struct usb_device *udev;					\
									\
	udev = to_usb_device (dev);					\
	return sprintf(buf, "%s\n", udev->name);			\
}									\
static DEVICE_ATTR(name, S_IRUGO, show_##name, NULL);

usb_string_attr(product);
usb_string_attr(manufacturer);
usb_string_attr(serial);

static ssize_t
show_speed (struct device *dev, struct device_attribute *attr, char *buf)
{
	struct usb_device *udev;
	char *speed;

	udev = to_usb_device (dev);

	switch (udev->speed) {
	case USB_SPEED_LOW:
		speed = "1.5";
		break;
	case USB_SPEED_UNKNOWN:
	case USB_SPEED_FULL:
		speed = "12";
		break;
	case USB_SPEED_HIGH:
		speed = "480";
		break;
	default:
		speed = "unknown";
	}
	return sprintf (buf, "%s\n", speed);
}
static DEVICE_ATTR(speed, S_IRUGO, show_speed, NULL);

static ssize_t
show_devnum (struct device *dev, struct device_attribute *attr, char *buf)
{
	struct usb_device *udev;

	udev = to_usb_device (dev);
	return sprintf (buf, "%d\n", udev->devnum);
}
static DEVICE_ATTR(devnum, S_IRUGO, show_devnum, NULL);

static ssize_t
show_version (struct device *dev, struct device_attribute *attr, char *buf)
{
	struct usb_device *udev;
	u16 bcdUSB;

	udev = to_usb_device(dev);
	bcdUSB = le16_to_cpu(udev->descriptor.bcdUSB);
	return sprintf(buf, "%2x.%02x\n", bcdUSB >> 8, bcdUSB & 0xff);
}
static DEVICE_ATTR(version, S_IRUGO, show_version, NULL);

static ssize_t
show_maxchild (struct device *dev, struct device_attribute *attr, char *buf)
{
	struct usb_device *udev;

	udev = to_usb_device (dev);
	return sprintf (buf, "%d\n", udev->maxchild);
}
static DEVICE_ATTR(maxchild, S_IRUGO, show_maxchild, NULL);

/* Descriptor fields */
#define usb_descriptor_attr_le16(field, format_string)			\
static ssize_t								\
show_##field (struct device *dev, struct device_attribute *attr,	\
		char *buf)						\
{									\
	struct usb_device *udev;					\
									\
	udev = to_usb_device (dev);					\
	return sprintf (buf, format_string, 				\
			le16_to_cpu(udev->descriptor.field));		\
}									\
static DEVICE_ATTR(field, S_IRUGO, show_##field, NULL);

usb_descriptor_attr_le16(idVendor, "%04x\n")
usb_descriptor_attr_le16(idProduct, "%04x\n")
usb_descriptor_attr_le16(bcdDevice, "%04x\n")

#define usb_descriptor_attr(field, format_string)			\
static ssize_t								\
show_##field (struct device *dev, struct device_attribute *attr,	\
		char *buf)						\
{									\
	struct usb_device *udev;					\
									\
	udev = to_usb_device (dev);					\
	return sprintf (buf, format_string, udev->descriptor.field);	\
}									\
static DEVICE_ATTR(field, S_IRUGO, show_##field, NULL);

#ifdef CONFIG_USB_HCD_TEST_MODE
//#include <linux/usb/ch11.h>
//#include <bspchip.h>
#include <platform.h>
#include <linux/slab.h>
//#include <mach/system.h>
//#include <rbus/usb_reg.h>
#include "hcd.h"

#define UsbTestModeNumber 6
#define UsbSuspendResume 1
#define UsbGetDescriptor 3

#define GET_MAPPED_RBUS_ADDR(x) (x)

#define BSP_EHCI_BASE       0xb8021000

#define EHCI_USBCMD (BSP_EHCI_BASE+0x10)
#define EHCI_USBSTS (BSP_EHCI_BASE+0x14)
#define EHCI_PORTSC (BSP_EHCI_BASE+0x54)

//#define HMAC_HOST_xhci_ctrl_USBCMD_reg 0xb8021010
//#define HMAC_HOST_xhci_ctrl_USBSTS_reg 0xb8021014
//#define HMAC_HOST_xhci_portsc_PORTPMSC_reg 0xb8021054
//#define pr_debug printk


int usb_test_mode_suspend_flag = 0;
EXPORT_SYMBOL(usb_test_mode_suspend_flag);
int gUsbHubSuspendResume[4] = {-1, -1, -1, -1}; /*  0:Resume, 1:Suspend */
EXPORT_SYMBOL(gUsbHubSuspendResume);

static int gUsbHubPortSuspendResume = 0; /*  0:Resume, 1:Suspend */
static int gUsbHubPort = 1; /* default = 1 */
static int gUsbHubTestMode = 0;
static int gUsbTestModeChanged = 0;

int gUsbGetDescriptor = 0;
/*  gUsbGetDescriptor = 1 for the command phase */
/*  gUsbGetDescriptor = 2 for the data phase */
/*  gUsbGetDescriptor = 3 for the status phase */

/*  copy from hub.c and rename */
/*
 * USB 2.0 spec Section 11.24.2.2
 */
static int hub_clear_port_feature(struct usb_device *hdev, int port1, int feature)
{
	return usb_control_msg(hdev, usb_sndctrlpipe(hdev, 0),
			ClearPortFeature, USB_RT_PORT, feature, port1,
			NULL, 0, 1000);
}


/*
 * USB 2.0 spec Section 11.24.2.13
 */
static int hub_set_port_feature(struct usb_device *hdev, int port1, int feature)
{
	return usb_control_msg(hdev, usb_sndctrlpipe(hdev, 0),
			SetPortFeature, USB_RT_PORT, feature, port1,
			NULL, 0, 1000);
}


static unsigned int volatile regs_addr = 0;
static ssize_t show_regs_addr(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%x\n", regs_addr);
}
static ssize_t
store_regs_addr(struct device *dev, struct device_attribute *attr,
		const char *buf, size_t count)
{
	int			config;

	if (sscanf(buf, "%x", &config) != 1)
		return -EINVAL;

	regs_addr = config;
	pr_debug("0x%x\n", regs_addr);
	return count;
}
static DEVICE_ATTR(regs_addr, S_IRUGO | S_IWUSR, show_regs_addr, store_regs_addr);

static ssize_t show_regs_value(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	if (regs_addr < 0xb8000000) {
		return 0;
        }else{
		return sprintf(buf, "0x%x = %x\n", regs_addr, *(unsigned int volatile*)GET_MAPPED_RBUS_ADDR(regs_addr));
        }
}
static ssize_t
store_regs_value(struct device *dev, struct device_attribute *attr,
		const char *buf, size_t count)
{
	int			config;

	if (sscanf(buf, "%x", &config) != 1)
		return -EINVAL;

	*(unsigned int volatile*)GET_MAPPED_RBUS_ADDR(regs_addr) = config;
	pr_debug("0x%x = 0x%x\n", regs_addr, *(unsigned int volatile*)GET_MAPPED_RBUS_ADDR(regs_addr));
	return count;
}
static DEVICE_ATTR(regs_value, S_IRUGO | S_IWUSR, show_regs_value, store_regs_value);

static ssize_t  show_bPortNumber (struct device *dev, struct device_attribute *attr, char *buf)
{
	struct usb_device *udev;
	struct usb_host_config *actconfig;

	udev = to_usb_device (dev);
	actconfig = udev->actconfig;

	if(udev->descriptor.bDeviceClass != USB_CLASS_HUB)
		return sprintf (buf, "%d\n", -1);

	return sprintf (buf, "%d\n", gUsbHubPort);

	return 0;
}

static ssize_t
set_bPortNumber (struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct usb_device	*udev = udev = to_usb_device (dev);
	int			config, value;

	if ((value = sscanf (buf, "%u", &config)) != 1 || config > udev->maxchild)
		return -EINVAL;

	if(udev->descriptor.bDeviceClass != USB_CLASS_HUB)
		return value;

	if(gUsbHubPort != config)
	{
		gUsbHubPort = config;
	}

	return (value < 0) ? value : count;
}

static DEVICE_ATTR(bPortNumber, S_IRUGO | S_IWUSR, show_bPortNumber, set_bPortNumber);

static ssize_t  show_bPortDescriptor (struct device *dev, struct device_attribute *attr, char *buf)
{
	struct usb_device *udev;
	struct usb_host_config *actconfig;

	udev = to_usb_device (dev);
	actconfig = udev->actconfig;

	return sprintf (buf, "%d\n", gUsbGetDescriptor);

	return 0;
}

extern int get_hub_descriptor_port(struct usb_device *hdev, void *data, int size, int port1);

static ssize_t
set_bPortDescriptor (struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct usb_device	*udev = udev = to_usb_device (dev);
	int			config, value;
	unsigned char		*data;
	int			size, i, port;
	int					  ret;

	if ((value = sscanf (buf, "%u", &config)) != 1 || config > UsbGetDescriptor)
		return -EINVAL;

	if(udev->descriptor.bDeviceClass != USB_CLASS_HUB)
		return sprintf ((char *)buf, "%d\n", -1);

	if((config == 0) || (gUsbGetDescriptor != config))
	{
		gUsbGetDescriptor = config;
		port = gUsbHubPort - 1;
		size=0x12;
		data = (unsigned char*)kmalloc(size, GFP_KERNEL);
		if (!data)
			return -ENOMEM;
		memset (data, 0, size);
		ret = get_hub_descriptor_port(udev, data, size, gUsbHubPort);

		if(gUsbGetDescriptor == 0)
		{
			pr_debug(" get device descriptor\n");
			for( i = 0; i < size; i++)
			{
				pr_debug(" %.2x", data[i]);
				if((i % 15) == 0 && (i != 0))
					pr_debug("\n<1>");
			}
			pr_debug("\n");
		}

		if(gUsbGetDescriptor == 3)
			gUsbGetDescriptor = 0;
		kfree(data);
	}

	return (value < 0) ? value : count;
}

static DEVICE_ATTR(bPortDescriptor, S_IRUGO | S_IWUSR,
		show_bPortDescriptor, set_bPortDescriptor);

/*
 * USB 2.0 spec Section 11.24.2.7
 */
static int hub_get_port_status(struct usb_device *hdev, int port1, unsigned char *buf, int length)
{
	return usb_control_msg(hdev, usb_rcvctrlpipe(hdev, 0),
			GetPortStatus, USB_RT_PORT | USB_DIR_IN, 0, port1,
			buf, length, 1000);
}

static int hub_check_port_suspend_resume(struct usb_device *hdev, int port1)
{
	unsigned char *data;
	int length = 4;
	int ret = -1;

	data = kmalloc(length, GFP_KERNEL);
	if(data == NULL)
		return ret;
	memset(data, 0, length);
	hub_get_port_status(hdev, port1, data, length);
	ret = (data[0] >> 2) & 0x1; /* bit 2 */
	kfree(data);

	return ret;
}


static ssize_t  show_bPortSuspendResume (struct device *dev, struct device_attribute *attr, char *buf)
{
	struct usb_device *udev;
	struct usb_host_config *actconfig;

	udev = to_usb_device (dev);
	actconfig = udev->actconfig;

	if(udev->descriptor.bDeviceClass != USB_CLASS_HUB)
		return sprintf (buf, "%d\n", -1);

	if(udev->parent == NULL) /*  root hub */
	{
		return sprintf (buf, "%d %d %d %d\n", gUsbHubSuspendResume[0], \
				gUsbHubSuspendResume[1], gUsbHubSuspendResume[2], gUsbHubSuspendResume[3]);
	}
	else /*  not root hub */
	{
		gUsbHubPortSuspendResume = hub_check_port_suspend_resume(udev, gUsbHubPort);
		return sprintf (buf, "%d\n", gUsbHubPortSuspendResume);
	}

	return 0;
}


static ssize_t
set_bPortSuspendResume (struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct usb_device	*udev = to_usb_device (dev);
	struct usb_hcd		*hcd = container_of(udev->bus, struct usb_hcd, self);
	int			config, value;
	unsigned int		port1;

	if ((value = sscanf (buf, "%u", &config)) != 1 || config > UsbSuspendResume)
		return -EINVAL;

	if(udev->descriptor.bDeviceClass != USB_CLASS_HUB)
		return value;

	if(udev->parent == NULL) /*  root hub */
	{
		port1 = (unsigned int) gUsbHubPort;
		if(gUsbHubSuspendResume[port1 - 1] != config)
		{
			gUsbHubSuspendResume[port1 - 1] = config;
			pr_debug("Root Hub port %d - %s\n", port1, (gUsbHubSuspendResume[port1 - 1] == 1) ? "Suspend" : "Resume");
			if(gUsbHubSuspendResume[port1 - 1] == 1) /* Suspend */
			{
				usb_test_mode_suspend_flag = 1;

				pr_debug("call bus_suspend() to the root hub port %d...\n", gUsbHubPort);
				usb_lock_device (hcd->self.root_hub);

				if (hcd->driver->bus_suspend) {
					hcd->driver->bus_suspend(hcd);
				} else {
//					pr_err("#@# %s(%d) hcd->driver->bus_suspend(hcd) is NULL!!! Need CONFIG_PM=y\n", __func__, __LINE__);
				}

				usb_unlock_device (hcd->self.root_hub);
				pr_debug("call bus_suspend() OK !!!\n");

			}
			else /* Resume */
			{
				usb_test_mode_suspend_flag = 0;

				pr_debug("call bus_resume() to the root hub port %d...\n", gUsbHubPort);
				usb_lock_device (hcd->self.root_hub);

				if (hcd->driver->bus_resume) {
					hcd->driver->bus_resume(hcd);
				} else {
					pr_debug("#@# %s(%d) hcd->driver->bus_resume(hcd) is NULL!!! Need CONFIG_PM=y\n", __func__, __LINE__);
				}

				usb_unlock_device (hcd->self.root_hub);
				pr_debug("call bus_resume() OK !!!\n");

			}
			msleep(1000);
		}
	}
	else /*  the hub which is not root hub */
	{
		gUsbHubPortSuspendResume = hub_check_port_suspend_resume(udev, gUsbHubPort);

		if(gUsbHubPortSuspendResume != config)
		{
			gUsbHubPortSuspendResume = config;
			if(gUsbHubPortSuspendResume == 1) /* Suspend */
			{
				pr_debug("set USB_PORT_FEAT_SUSPEND to the port %d of the hub ...\n", gUsbHubPort);
				hub_set_port_feature(udev, gUsbHubPort, USB_PORT_FEAT_SUSPEND);
				pr_debug("set OK !!!\n");
			}
			else /* Resume */
			{
				pr_debug("clear USB_PORT_FEAT_SUSPEND to the port %d of the hub ...\n", gUsbHubPort);
				hub_clear_port_feature(udev, gUsbHubPort, USB_PORT_FEAT_SUSPEND);
				pr_debug("clear OK !!!\n");
			}
			msleep(1000);
		}
	}

	return (value < 0) ? value : count;
}

static DEVICE_ATTR(bPortSuspendResume, S_IRUGO | S_IWUSR,
		show_bPortSuspendResume, set_bPortSuspendResume);


static ssize_t  show_bPortSuspendResume_ctrl (struct device *dev, struct device_attribute *attr, char *buf)
{
	struct usb_device *udev;
	struct usb_host_config *actconfig;

	udev = to_usb_device (dev);
	actconfig = udev->actconfig;

	if(udev->descriptor.bDeviceClass != USB_CLASS_HUB)
		return sprintf (buf, "%d\n", -1);

	if(udev->parent == NULL) /*  root hub */
	{
		return sprintf (buf, "%d %d %d %d\n", gUsbHubSuspendResume[0], \
				gUsbHubSuspendResume[1], gUsbHubSuspendResume[2], gUsbHubSuspendResume[3]);
	}
	else /*  not root hub */
	{
		gUsbHubPortSuspendResume = hub_check_port_suspend_resume(udev, gUsbHubPort);
		return sprintf (buf, "%d\n", gUsbHubPortSuspendResume);
	}

	return 0;
}

static ssize_t
set_bPortSuspendResume_ctrl (struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct usb_device	*udev = to_usb_device (dev);
	struct usb_hcd		*hcd = container_of(udev->bus, struct usb_hcd, self);
	int			config, value;
	unsigned int		port1;

	if ((value = sscanf (buf, "%u", &config)) != 1 || config > UsbSuspendResume)
		return -EINVAL;

	if(udev->descriptor.bDeviceClass != USB_CLASS_HUB)
		return value;

	if(udev->parent == NULL) /*  root hub */
	{
		port1 = (unsigned int) gUsbHubPort;
		/* if(gUsbHubSuspendResume[port1 - 1] != config) */
		{
			gUsbHubSuspendResume[port1 - 1] = config;
			pr_debug("Root Hub port %d - %s\n", port1, (gUsbHubSuspendResume[port1 - 1] == 1) ? "Suspend" : "Resume");
			if(gUsbHubSuspendResume[port1 - 1] == 1) /* Suspend */
			{
				usb_test_mode_suspend_flag = 1;

				pr_debug("call bus_suspend() to the root hub port %d...\n", gUsbHubPort);
				hub_set_port_feature(udev, gUsbHubPort, USB_PORT_FEAT_SUSPEND);
				pr_debug("call bus_suspend() OK !!!\n");

			}
			else /* Resume */
			{
				usb_test_mode_suspend_flag = 0;

				pr_debug("call bus_resume() to the root hub port %d...\n", gUsbHubPort);
				hub_clear_port_feature(udev, gUsbHubPort, USB_PORT_FEAT_SUSPEND);
				pr_debug("call bus_resume() OK !!!\n");

			}
			msleep(1000);
		}
	}
	else /*  the hub which is not root hub */
	{
		gUsbHubPortSuspendResume = hub_check_port_suspend_resume(udev, gUsbHubPort);

		/* if(gUsbHubPortSuspendResume != config) */
		{
			gUsbHubPortSuspendResume = config;
			if(gUsbHubPortSuspendResume == 1) /* Suspend */
			{
				pr_debug("set USB_PORT_FEAT_SUSPEND to the port %d of the hub ...\n", gUsbHubPort);
				hub_set_port_feature(udev, gUsbHubPort, USB_PORT_FEAT_SUSPEND);
				pr_debug("set OK !!!\n");
			}
			else /* Resume */
			{
				pr_debug("clear USB_PORT_FEAT_SUSPEND to the port %d of the hub ...\n", gUsbHubPort);
				hub_clear_port_feature(udev, gUsbHubPort, USB_PORT_FEAT_SUSPEND);
				pr_debug("clear OK !!!\n");
			}
			msleep(1000);
		}
	}

	return (value < 0) ? value : count;
}

static DEVICE_ATTR(bPortSuspendResume_ctrl, S_IRUGO | S_IWUSR,
		show_bPortSuspendResume_ctrl, set_bPortSuspendResume_ctrl);


#if 1//defined(CONFIG_ARCH_RTKS2B) || defined(CONFIG_ARCH_RTK299S) || defined(CONFIG_ARCH_RTK299O) || defined(CONFIG_ARCH_RTK289X)
static ssize_t  show_bPortTestMode (struct device *dev, struct device_attribute *attr, char *buf)
{
	struct usb_device *udev;
	struct usb_host_config *actconfig;

	udev = to_usb_device (dev);
	actconfig = udev->actconfig;

	if(udev->descriptor.bDeviceClass != USB_CLASS_HUB)
		return sprintf (buf, "%d\n", -1);

	return sprintf (buf, "%d\n", gUsbHubTestMode);

	return 0;
}
#endif /* defined(CONFIG_ARCH_RTKS2B) || defined(CONFIG_ARCH_RTK299S) || defined(CONFIG_ARCH_RTK299O) || defined(CONFIG_ARCH_RTK289X) */

#if 1//defined(CONFIG_ARCH_RTK299S) || defined(CONFIG_ARCH_RTK299O) || defined(CONFIG_ARCH_RTK289X)
static ssize_t
set_bPortTestMode (struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	struct usb_device	*udev = udev = to_usb_device (dev);
	int			config, value, i, prev_config = -1;
	unsigned int		port1 = 0;
	unsigned int tmp;
	unsigned int volatile addr;

	if ((value = sscanf (buf, "%u", &config)) != 1 || config > UsbTestModeNumber)
		return -EINVAL;

	if(udev->descriptor.bDeviceClass != USB_CLASS_HUB)
		return value;

	if(config != 6)
	{
		if(gUsbHubTestMode != config)
		{
			if(!((gUsbHubTestMode > 0) && (config > 0)))
			{
				prev_config = gUsbHubTestMode;
				gUsbHubTestMode = config;
				gUsbTestModeChanged = 1;
			}
		}
		else
		{
			if(config == 0)
				gUsbTestModeChanged = 1;
		}
		port1 = (unsigned int) gUsbHubPort;
	}

	if(gUsbTestModeChanged == 1)
	{
		gUsbTestModeChanged = 0;

		if(config != 0)
		{
			pr_debug("Call usb_lock_device().\n");
			usb_lock_device(udev);
		}
		#define EHCI_STS_HALTED (1<<12)
		if(udev->parent == NULL) /* root hub */
		{
			if(config == 0)
			{
				pr_debug("Leave test mode ...\n");

				pr_debug("set the Run/Stop(R/S) bit in the USBCMD register to a '0'\n");
				tmp = le32_to_cpu(REG32(EHCI_USBCMD));//tmp = *(unsigned int volatile *)GET_MAPPED_RBUS_ADDR(HMAC_HOST_xhci_ctrl_USBCMD_reg);
				tmp &= ~(0x1);
				REG32(EHCI_USBCMD) = cpu_to_le32(tmp); //*(unsigned int volatile *)GET_MAPPED_RBUS_ADDR(HMAC_HOST_xhci_ctrl_USBCMD_reg) = tmp;

				pr_debug("wait for HCHalted(HCH) bit in the USBSTS register to transition to a '1'\n");
				do {
					msleep(100);
					tmp = le32_to_cpu(REG32(EHCI_USBSTS));//tmp = *(unsigned int volatile *)GET_MAPPED_RBUS_ADDR(HMAC_HOST_xhci_ctrl_USBSTS_reg);
				} while ((tmp & EHCI_STS_HALTED) != EHCI_STS_HALTED);

				pr_debug("set the Host Controller Reset(HCRST) bit in the USBCMD register to a '1'\n");
				tmp = le32_to_cpu(REG32(EHCI_USBCMD));//tmp = *(unsigned int volatile *)GET_MAPPED_RBUS_ADDR(HMAC_HOST_xhci_ctrl_USBCMD_reg);
				tmp |= (0x1 << 1);
				REG32(EHCI_USBCMD) = cpu_to_le32(tmp); //*(unsigned int volatile *)GET_MAPPED_RBUS_ADDR(HMAC_HOST_xhci_ctrl_USBCMD_reg) = tmp;

				pr_debug("Leave test mode , OK !!!\n");
			}
			else
			{
				pr_debug("Enter test mode ...\n");

				pr_debug("clear USB_PORT_FEAT_POWER to the parent of the hub\n");
				for (i=1;i<=udev->maxchild;i++)
				{
					pr_debug("processing port %d of %d...\n", i, udev->maxchild);
					hub_clear_port_feature(udev, i, USB_PORT_FEAT_POWER);
					msleep(1000);
				}

				pr_debug("set the Run/Stop(R/S) bit in the USBCMD register to a '0'\n");
				tmp = le32_to_cpu(REG32(EHCI_USBCMD));//tmp = *(unsigned int volatile *)GET_MAPPED_RBUS_ADDR(HMAC_HOST_xhci_ctrl_USBCMD_reg);
				tmp &= ~(0x1);
				REG32(EHCI_USBCMD) = cpu_to_le32(tmp); //*(unsigned int volatile *)GET_MAPPED_RBUS_ADDR(HMAC_HOST_xhci_ctrl_USBCMD_reg) = tmp;

				pr_debug("wait for HCHalted(HCH) bit in the USBSTS register to transition to a '1'\n");
				do {
					msleep(100);
					tmp = le32_to_cpu(REG32(EHCI_USBSTS));//tmp = *(unsigned int volatile *)GET_MAPPED_RBUS_ADDR(HMAC_HOST_xhci_ctrl_USBSTS_reg);
				} while ((tmp & EHCI_STS_HALTED) != EHCI_STS_HALTED);

				pr_debug("set test mode %d to port %d ...\n", config, port1);
				addr = EHCI_PORTSC + (port1 - 1) * 4; //addr = HMAC_HOST_xhci_portsc_PORTPMSC_reg + (port1 - 1) * 0x10;
				tmp = le32_to_cpu(REG32(addr));//tmp = *(unsigned int volatile *)GET_MAPPED_RBUS_ADDR(addr);
				tmp = (tmp & ~(0xf << 16)) | (config << 16); //tmp |= (config << 28);
				printk("W %x to %x\n",tmp,addr);
				REG32(addr) = cpu_to_le32(tmp); // *(unsigned int volatile *)GET_MAPPED_RBUS_ADDR(addr) = tmp;

				if (config == 5) {
					pr_debug("set the Run/Stop(R/S) bit in the USBCMD register to a '1'\n");
					tmp = le32_to_cpu(REG32(EHCI_USBCMD));//tmp = *(unsigned int volatile *)GET_MAPPED_RBUS_ADDR(HMAC_HOST_xhci_ctrl_USBCMD_reg);
					tmp |= (0x1);
					REG32(EHCI_USBCMD) = cpu_to_le32(tmp); //*(unsigned int volatile *)GET_MAPPED_RBUS_ADDR(HMAC_HOST_xhci_ctrl_USBCMD_reg) = tmp;
				}

				msleep(1000);

				pr_debug("Enter test mode , OK !!!\n");
			}
		}
		else /* not root hub */
		{
			if(config == 0)
			{
				pr_debug("Leave test mode ...\n");

				pr_debug("clear USB_PORT_FEAT_POWER to the parent of the hub\n");
				for (i=1;i<=udev->parent->maxchild;i++)
				{
					pr_debug("processing port %d of %d...\n", i, udev->parent->maxchild);
					hub_clear_port_feature(udev->parent, i, USB_PORT_FEAT_POWER);
					msleep(1000);
				}

				pr_debug("set USB_PORT_FEAT_POWER to the parent of the hub\n");
				for (i=1;i<=udev->parent->maxchild;i++)
				{
					pr_debug("processing port %d of %d...\n", i, udev->parent->maxchild);
					hub_set_port_feature(udev->parent, i, USB_PORT_FEAT_POWER);
					msleep(1000);
				}

				pr_debug("Leave test mode , OK !!!\n");
			}
			else
			{
				pr_debug("Enter test mode ...\n");

				pr_debug("set USB_PORT_FEAT_SUSPEND to all ports of the hub\n");
				for (i=1;i<=udev->maxchild;i++)
				{
					pr_debug("processing port %d of %d...\n", i, udev->maxchild);
					hub_set_port_feature(udev, i, USB_PORT_FEAT_SUSPEND);
					msleep(1000);
				}

				pr_debug("set USB_PORT_FEAT_TEST mode %d to port %d ...\n", config, port1);
				hub_set_port_feature(udev,(config << 8) | port1, USB_PORT_FEAT_TEST);
				msleep(1000);

				pr_debug("Enter test mode , OK !!!\n");
			}
		}

		if(config == 0)
		{
			pr_debug("Call usb_unlock_device().\n");
			usb_unlock_device(udev);
		}
	}

	return (value < 0) ? value : count;
}


static DEVICE_ATTR(bPortTestMode, S_IRUGO | S_IWUSR,
		show_bPortTestMode, set_bPortTestMode);
#endif /* defined(CONFIG_ARCH_RTK299S) || defined(CONFIG_ARCH_RTK299O) || defined(CONFIG_ARCH_RTK289X) */


#endif /* CONFIG_USB_HCD_TEST_MODE */

usb_descriptor_attr (bDeviceClass, "%02x\n")
usb_descriptor_attr (bDeviceSubClass, "%02x\n")
usb_descriptor_attr (bDeviceProtocol, "%02x\n")
usb_descriptor_attr (bNumConfigurations, "%d\n")
usb_descriptor_attr (bMaxPacketSize0, "%d\n")

static struct attribute *dev_attrs[] = {
	/* current configuration's attributes */
	&dev_attr_configuration.attr,
	&dev_attr_bNumInterfaces.attr,
	&dev_attr_bConfigurationValue.attr,
	&dev_attr_bmAttributes.attr,
	&dev_attr_bMaxPower.attr,
	/* device attributes */
	&dev_attr_idVendor.attr,
	&dev_attr_idProduct.attr,
	&dev_attr_bcdDevice.attr,
	&dev_attr_bDeviceClass.attr,
	&dev_attr_bDeviceSubClass.attr,
	&dev_attr_bDeviceProtocol.attr,
	&dev_attr_bNumConfigurations.attr,
	&dev_attr_bMaxPacketSize0.attr,
	&dev_attr_speed.attr,
	&dev_attr_devnum.attr,
	&dev_attr_version.attr,
	&dev_attr_maxchild.attr,
#ifdef CONFIG_USB_HCD_TEST_MODE
	&dev_attr_regs_addr.attr,
	&dev_attr_regs_value.attr,
	&dev_attr_bPortNumber.attr,
	&dev_attr_bPortDescriptor.attr,
	&dev_attr_bPortSuspendResume.attr,
	&dev_attr_bPortSuspendResume_ctrl.attr,
	&dev_attr_bPortTestMode.attr,
#endif /* CONFIG_USB_HCD_TEST_MODE */
	NULL,
};
static struct attribute_group dev_attr_grp = {
	.attrs = dev_attrs,
};

int usb_create_sysfs_dev_files(struct usb_device *udev)
{
	struct device *dev = &udev->dev;
	int retval;

	retval = sysfs_create_group(&dev->kobj, &dev_attr_grp);
	if (retval)
		return retval;

	if (udev->manufacturer) {
		retval = device_create_file (dev, &dev_attr_manufacturer);
		if (retval)
			goto error;
	}
	if (udev->product) {
		retval = device_create_file (dev, &dev_attr_product);
		if (retval)
			goto error;
	}
	if (udev->serial) {
		retval = device_create_file (dev, &dev_attr_serial);
		if (retval)
			goto error;
	}
	retval = usb_create_ep_files(dev, &udev->ep0, udev);
	if (retval)
		goto error;
	return 0;
error:
	usb_remove_ep_files(&udev->ep0);
	device_remove_file(dev, &dev_attr_manufacturer);
	device_remove_file(dev, &dev_attr_product);
	device_remove_file(dev, &dev_attr_serial);
	return retval;
}

void usb_remove_sysfs_dev_files (struct usb_device *udev)
{
	struct device *dev = &udev->dev;

	usb_remove_ep_files(&udev->ep0);
	sysfs_remove_group(&dev->kobj, &dev_attr_grp);

	if (udev->manufacturer)
		device_remove_file(dev, &dev_attr_manufacturer);
	if (udev->product)
		device_remove_file(dev, &dev_attr_product);
	if (udev->serial)
		device_remove_file(dev, &dev_attr_serial);
}

/* Interface fields */
#define usb_intf_attr(field, format_string)				\
static ssize_t								\
show_##field (struct device *dev, struct device_attribute *attr,	\
		char *buf)						\
{									\
	struct usb_interface *intf = to_usb_interface (dev);		\
									\
	return sprintf (buf, format_string,				\
			intf->cur_altsetting->desc.field); 		\
}									\
static DEVICE_ATTR(field, S_IRUGO, show_##field, NULL);

usb_intf_attr (bInterfaceNumber, "%02x\n")
usb_intf_attr (bAlternateSetting, "%2d\n")
usb_intf_attr (bNumEndpoints, "%02x\n")
usb_intf_attr (bInterfaceClass, "%02x\n")
usb_intf_attr (bInterfaceSubClass, "%02x\n")
usb_intf_attr (bInterfaceProtocol, "%02x\n")

static ssize_t show_interface_string(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct usb_interface *intf;
	struct usb_device *udev;
	int len;

	intf = to_usb_interface (dev);
	udev = interface_to_usbdev (intf);
	len = snprintf(buf, 256, "%s", intf->cur_altsetting->string);
	if (len < 0)
		return 0;
	buf[len] = '\n';
	buf[len+1] = 0;
	return len+1;
}
static DEVICE_ATTR(interface, S_IRUGO, show_interface_string, NULL);

static ssize_t show_modalias(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct usb_interface *intf;
	struct usb_device *udev;
	struct usb_host_interface *alt;

	intf = to_usb_interface(dev);
	udev = interface_to_usbdev(intf);
	alt = intf->cur_altsetting;

	return sprintf(buf, "usb:v%04Xp%04Xd%04Xdc%02Xdsc%02Xdp%02X"
			"ic%02Xisc%02Xip%02X\n",
			le16_to_cpu(udev->descriptor.idVendor),
			le16_to_cpu(udev->descriptor.idProduct),
			le16_to_cpu(udev->descriptor.bcdDevice),
			udev->descriptor.bDeviceClass,
			udev->descriptor.bDeviceSubClass,
			udev->descriptor.bDeviceProtocol,
			alt->desc.bInterfaceClass,
			alt->desc.bInterfaceSubClass,
			alt->desc.bInterfaceProtocol);
}
static DEVICE_ATTR(modalias, S_IRUGO, show_modalias, NULL);

static struct attribute *intf_attrs[] = {
	&dev_attr_bInterfaceNumber.attr,
	&dev_attr_bAlternateSetting.attr,
	&dev_attr_bNumEndpoints.attr,
	&dev_attr_bInterfaceClass.attr,
	&dev_attr_bInterfaceSubClass.attr,
	&dev_attr_bInterfaceProtocol.attr,
	&dev_attr_modalias.attr,
	NULL,
};
static struct attribute_group intf_attr_grp = {
	.attrs = intf_attrs,
};

static inline void usb_create_intf_ep_files(struct usb_interface *intf,
		struct usb_device *udev)
{
	struct usb_host_interface *iface_desc;
	int i;

	iface_desc = intf->cur_altsetting;
	for (i = 0; i < iface_desc->desc.bNumEndpoints; ++i)
		usb_create_ep_files(&intf->dev, &iface_desc->endpoint[i],
				udev);
}

static inline void usb_remove_intf_ep_files(struct usb_interface *intf)
{
	struct usb_host_interface *iface_desc;
	int i;

	iface_desc = intf->cur_altsetting;
	for (i = 0; i < iface_desc->desc.bNumEndpoints; ++i)
		usb_remove_ep_files(&iface_desc->endpoint[i]);
}

int usb_create_sysfs_intf_files(struct usb_interface *intf)
{
	struct usb_device *udev = interface_to_usbdev(intf);
	struct usb_host_interface *alt = intf->cur_altsetting;
	int retval;

	retval = sysfs_create_group(&intf->dev.kobj, &intf_attr_grp);
	if (retval)
		goto error;

	if (alt->string == NULL)
		alt->string = usb_cache_string(udev, alt->desc.iInterface);
	if (alt->string)
		retval = device_create_file(&intf->dev, &dev_attr_interface);
	usb_create_intf_ep_files(intf, udev);
	return 0;
error:
	if (alt->string)
		device_remove_file(&intf->dev, &dev_attr_interface);
	sysfs_remove_group(&intf->dev.kobj, &intf_attr_grp);
	usb_remove_intf_ep_files(intf);
	return retval;
}

void usb_remove_sysfs_intf_files (struct usb_interface *intf)
{
	usb_remove_intf_ep_files(intf);
	sysfs_remove_group(&intf->dev.kobj, &intf_attr_grp);

	if (intf->cur_altsetting->string)
		device_remove_file(&intf->dev, &dev_attr_interface);
}
